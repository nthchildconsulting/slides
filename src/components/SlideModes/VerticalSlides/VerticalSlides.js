import React from 'react';
import Slide from 'components/Slide/Slide';

const VerticalSlides = ({ slides, show }) => {
  if (!show) {
    return <></>;
  }
  return (
    <>
      {slides.map((slide, key) => (
        <Slide key={key} lastSlide={key === slides.length - 2}>
          {slide}
        </Slide>
      ))}
    </>
  );
};

export default VerticalSlides;
