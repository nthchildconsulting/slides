import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ArrowLeft from '@material-ui/icons/ArrowLeft';
import ArrowRight from '@material-ui/icons/ArrowRight';
import IconButton from '@material-ui/core/IconButton';
import Slide from 'components/Slide/Slide';

const styles = {
  controls: {
    position: 'fixed',
    top: '50%',
    left: 5,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
  },
};

const HortizontalSlides = ({ classes, slides, show }) => {
  if (!show) {
    return <></>;
  }
  const [activeSlide, setActiveSlide] = useState(0);
  return (
    <>
      <Slide>{slides[activeSlide]}</Slide>
      <div className={classes.controls}>
        <IconButton
          disabled={activeSlide === 0}
          onClick={() => setActiveSlide(activeSlide - 1)}
        >
          <ArrowLeft fontSize="large" />
        </IconButton>
        <IconButton
          disabled={activeSlide === slides.length - 1}
          onClick={() => setActiveSlide(activeSlide + 1)}
        >
          <ArrowRight fontSize="large" />
        </IconButton>
      </div>
    </>
  );
};

HortizontalSlides.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(HortizontalSlides);
