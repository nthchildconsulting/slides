import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    margin: 0,
    padding: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: theme.slide.height,
  },
  slide: {
    margin: 0,
    padding: 0,
    maxWidth: theme.slide.width,
    maxHeight: theme.slide.height,
    border: '1px solid black',
  },
});

const Slide = ({ classes, children, lastSlide }) => {
  const style = lastSlide
    ? {}
    : {
        pageBreakAfter: 'always',
      };
  return (
    <div className={classes.root} style={style}>
      <div className={classes.slide}>{children}</div>
    </div>
  );
};

Slide.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.object.isRequired,
};

export default withStyles(styles)(Slide);
