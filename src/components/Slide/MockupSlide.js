import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    margin: 0,
    padding: 0,
    height: theme.slide.height,
  },
  img: {
    margin: 0,
    padding: 0,
    maxWidth: theme.slide.width,
    maxHeight: theme.slide.height,
    width: '100%',
    height: '100%',
  },
});

const Mockupslide = ({ classes, index }) => {
  const src = require(`assets/img/mockup/${index}.jpg`);
  return (
    <div className={classes.root}>
      <img src={src} alt={index} className={classes.img} />
    </div>
  );
};

Mockupslide.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Mockupslide);
