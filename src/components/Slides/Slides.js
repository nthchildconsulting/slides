import React, { useContext } from 'react';
import SettingsContext from 'context/Settings/SettingsContext';
import VerticalSlides from 'components/SlideModes/VerticalSlides/VerticalSlides';
import HorizontalSlides from 'components/SlideModes/HorizontalSlides/HorizontalSlides';
import MockupSlide from 'components/Slide/MockupSlide';
// import Compare from 'components/Slides/Compare/Compare';
// import Cover from 'components/Slides/Cover/Cover';
// import Payment from 'components/Slides/Payment/Payment';
// import RoofDesign from 'components/Slides/RoofDesign/RoofDesign';
// import RoofDetails from 'components/Slides/RoofDetails/RoofDetails';
// import Summary from 'components/Slides/Summary/Summary';
// import Ventilation from 'components/Slides/Ventilation/Ventilation';
// import Warranty from 'components/Slides/Warranty/Warranty';

// const slides = [
//   <Cover />,
//   <Warranty />,
//   <RoofDetails />,
//   <Ventilation />,
//   <Compare />,
//   <RoofDesign />,
//   <Summary />,
//   <Payment />,
// ];

const mockupSlides = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].map(
  index => {
    return <MockupSlide index={index} />;
  }
);

const Slides = () => {
  const context = useContext(SettingsContext);
  return (
    <>
      <VerticalSlides
        slides={mockupSlides}
        show={context.slideMode === 'vertical'}
      />
      <HorizontalSlides
        slides={mockupSlides}
        show={context.slideMode === 'horizontal'}
      />
    </>
  );
};

export default Slides;
