import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
// import defaultTheme from 'themes/default';
import darkTheme from 'themes/dark';
import RightDrawer from 'components/RightDrawer/RightDrawer';
import SettingsContext from 'context/Settings/SettingsContext';
import Slides from 'components/Slides/Slides';

const App = () => {
  // const context = useContext(SettingsContext);
  const theme = darkTheme;
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <RightDrawer />
      <Slides />
    </MuiThemeProvider>
  );
};

export default App;
