import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    type: 'light',
  },
  slide: {
    width: '297mm',
    height: '209mm',
  },
});

export default theme;
