import React from 'react';

export default React.createContext({
  slideMode: 'vertical',
  rightDrawerOpen: false,
  darkMode: false,
});
