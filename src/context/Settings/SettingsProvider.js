import React, { useState } from 'react';
import SettingsContext from './SettingsContext';

const storage = {
  getItem(key) {
    if (localStorage) {
      return localStorage.getItem(key);
    }
  },
  setItem(key, value) {
    if (localStorage) {
      return localStorage.setItem(key, value);
    }
  },
};

const SettingsProvider = props => {
  const [slideMode, setSlideMode] = useState(
    storage.getItem('slideMode') || 'vertical'
  );
  const [darkMode, setDarkMode] = useState(
    storage.getItem('darkMode') === 'true'
  );
  const onSetSlideMode = slideMode => {
    setSlideMode(slideMode);
  };
  const onSetDarkMode = darkMode => {
    setDarkMode(darkMode);
    storage.setItem('darkMode', darkMode);
  };
  return (
    <SettingsContext.Provider
      value={{
        slideMode,
        darkMode,
        onSetSlideMode,
        onSetDarkMode,
      }}
    >
      {props.children}
    </SettingsContext.Provider>
  );
};

export default SettingsProvider;
