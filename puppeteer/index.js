const puppeteer = require('puppeteer');
const path = require('path');

const URL = 'http://localhost:3000';
const SAVE_PATH = path.join(__dirname, 'screenshots', 'proposal.pdf');

(async () => {
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(URL);
    await page.pdf({
      path: SAVE_PATH,
      format: 'A4',
      printBackground: true,
      landscape: true,
      preferCSSPageSize: true,
    });
    await browser.close();
  } catch (e) {
    console.log(e);
  } finally {
    process.exit();
  }
})();
